import re
from flask_restful import Api, Resource, reqparse, inputs
from app.logs.models import db, Log, Person


api = Api()

parser = reqparse.RequestParser()
parser.add_argument('from', type=inputs.positive, location='args', required=True)
parser.add_argument('to', type=inputs.positive, location='args', required=True)
parser.add_argument('page', type=inputs.positive, location='args', required=True)
parser.add_argument('items', type=inputs.positive, location='args', required=True)


def idrepl(matchobj):
    person_id = int(matchobj.group(1))
    person = Person.query.get(person_id)
    return person.first_name + ' ' + person.last_name


def process_logs(logs):
    if not logs:
        return logs

    output = []
    for row in logs:
        log = dict()
        log['date'] = int(row[0])
        log['event'] = re.sub(r'<p=(\d+)>', idrepl, row[1])
        output.append(log)

    return output


class Logs(Resource):

    def get(self):
        args = parser.parse_args()
        if args['from'] > args['to']:
            return {'message':
                        {'to': 'Invalid to: \'to\' must be greater or equal than \'from\''}
                    }
        logs = db.session\
            .query(Log.dt.label('date'), Log.event)\
            .filter(Log.dt.between(args['from'], args['to']))\
            .order_by(Log.dt)
        total_logs = logs.count()
        max_page = total_logs // args['items'] + 1
        if args['page'] > max_page:
            return {'message':
                        {'page': 'Invalid page: maximum page is {}'.format(max_page)}
                    }
        logs = logs\
            .paginate(page=args['page'], per_page=args['items'])\
            .items
        logs = process_logs(logs)
        return logs


api.add_resource(Logs, '/logs')
