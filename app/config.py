import os


BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


class BaseConfig:
    DEBUG = False
    TESTING = False
    SQLALCHEMY_BINDS = {
        'logs': 'sqlite:///' + os.path.join(BASE_DIR, 'logs.db'),
        'people': 'sqlite:///' + os.path.join(BASE_DIR, 'people.db')
    }
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class DevelopmentConfig(BaseConfig):
    DEBUG = True
    SECRET_KEY = 'development'


class TestingConfig(BaseConfig):
    TESTING = True
    SECRET_KEY = 'testing'


class ProductionConfig(BaseConfig):
    DEBUG = False


app_config = {
    'development': DevelopmentConfig,
    'testing': TestingConfig,
    'production': ProductionConfig,
}
