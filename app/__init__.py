from flask import Flask
from .config import app_config


def create_app(config_name):
    if config_name not in app_config:
        raise ValueError('Incorrect config name. Valid are: {}'.format(list(app_config.keys())))

    app = Flask(__name__)
    app.config.from_object(app_config[config_name])

    from app.logs import db
    db.init_app(app)

    from app.api import api
    api.init_app(app)

    return app
